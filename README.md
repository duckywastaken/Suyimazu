<p align="center">
  <img src="https://codeberg.org/Alexander88207/Suyimazu/raw/branch/main/Media/Banner.png">
</p>

<p align="center">
	<a href="https://codeberg.org/Alexander88207/Suyimazu/src/branch/main/Library">Library</a>&nbsp;&nbsp;&nbsp;
	<a href="https://codeberg.org/Alexander88207/Suyimazu/src/branch/main/Docs/Working-Games.md">Working Games</a>&nbsp;&nbsp;&nbsp;
	<a href="https://codeberg.org/Alexander88207/Suyimazu/src/branch/main/Docs/Handbook.md">Handbook</a>&nbsp;&nbsp;&nbsp;
	<a href="https://codeberg.org/Alexander88207/Suyimazu/src/branch/main/Docs/Contributing.md">Contributing</a>
</p>

<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong>Description</strong></p>

<p align="center">
Suyimazu is a launcher that aims to make Windows applications (especially from the gaming sector)
<br>executable under Wine for any user.<br>

<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong>Installation</strong></p>

<p align="center">
To install Suyimazu from the FreeBSD repositories along with all dependencies, execute:
<br><code>pkg install suyimazu</code>

<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong>Uninstallation</strong></p>

<p align="center">
To uninstall Suyimazu along with all dependencies, execute:
<br><code>pkg uninstall suyimazu</code>
<br><code>pkg autoremove</code></p>

<p align="center">
To also clear Suyimazu's user data:
<br><code>rm -r -d $HOME/.local/share/Suyimazu</code></p>

<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong>Support</strong></p>
  <p align="center">
  If you have any problems, suggestions etc.. please open an issue here.
  <br>
  Alternatively you can reach me at:</p>
  <p align="center">
<a href="https://discord.com/invite/n2wshsy">FreeBSD Discord</a>
<br>
<a href="https://matrix.to/#/#FreeBSD:matrix.org?via=matrix.org&via=privacytools.io&via=kde.org">FreeBSD Matrix Group</a>
<br>
<a href="https://steamcommunity.com/groups/FreeBSD-Community">FreeBSD Community Steam Group</a></p>


<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong> Former contributors</strong></p>
  <p align="center">
    Brandon Ayers - Testing games
    <br>Epychan - Testing games and reporting fixes
    <br>MagZu - Hosting files in the past
    <br>Rvtsv - Testing games
    <br>shiorid - Testing games</p>

<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong>Special Thanks</strong></p>
  <p align="center">
    Special thanks to the main maintainers who make Wine on FreeBSD possible
    <br>and to those who participate in it.<br>

<p align="center">
<small>━━━━━━━━━━━━━━━━━━━━━━━━━━━◇◆◇━━━━━━━━━━━━━━━━━━━━━━━━━━━</small></p>

<p align="center">
  <strong>License</strong></p>

<p align="center">
The application itself is licensed under the BSD 2-Clause, 
<br>
<a href="https://pipiwiki.com/wiki/63rd_Street_Shuttle">the logo</a> is licensed under CC BY-SA 4.0.<br></p>

<p align="center">
<img src="https://codeberg.org/Alexander88207/Suyimazu/raw/branch/main/Media/Screenshot.png" alt="Screenshot">
<br>
<img src="https://codeberg.org/Alexander88207/Suyimazu/raw/branch/main/Media/Screenshot2.png" alt="Screenshot2"></p>
