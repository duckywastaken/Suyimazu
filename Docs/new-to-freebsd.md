# New to FreeBSD

Welcome to the quick guide how to install FreeBSD and setup an desktop for multimeda/gaming.

Please note that is only a small peace of the big cake.

If you want to know more please take a view at the FreeBSD Handbook: https://docs.freebsd.org/en/books/handbook/

# Installing FreeBSD

What you choose during the FreeBSD installation, use and so on, is up to you. But I recommend to check disable_sendmail in the service menu which comes with the installation. 

You can read up here what sendmail is: https://www.freebsd.org/cgi/man.cgi?query=sendmail&apropos=0&sektion=8&manpath=FreeBSD+13.0-RELEASE+and+Ports&arch=default&format=html

# Tweaks after installation

## TRIM (Recommended)

When you are using SSD you want to enabled TRIM.

When using ZFS, TRIM is enabled by default so can just skip that part here.

When using UFS, TRIM is not enabled by default. To enabled it go into the single-user mode and use tunefs to enabled trim.

To enable TRIM on an UFS partition in my case a standalone single-disk one partition (default ufs) system it would be: `tunefs -t enable /dev/ada0p2`.

After enabling it just reboot and then you can continue use your FreeBSD system in the multi-user mode.

## One more UFS tweak (Recommended)

I do have found that soft journaling from UFS causes stuttering in some games.

You can turn it off by going into the single-user mode and use tunefs to disable soft journaling.

To disable soft journaling on an UFS partition in my case a standalone single-disk one partition (default ufs) system it would be: `tunefs -j disable /dev/ada0p2`.

# Installing of GPU drivers.

The GPU wiki is well documented is here: https://wiki.freebsd.org/Graphics

# Installation of an desktop enviroment.

## Picking an enviroment

When using Wine with DXVK there are currently problems under GNOME based desktops and KDE. This means that the game window just closes at the beginning without any helpful error message (neither in the log nor as a gui).

This does not affect every game, but has been observed with Sea of Thieves, Life is Strange 2 and Elder Scrolls Online.

You can use: XFCE, LXQt, Metacity, IceWM, JWM, Openbox etc..

In this tutorial i do only cover now an XFCE installation:

Before we are beginning an installation of an enviroment you may also want to install an displaymanager aka loginscreen first: As example we install now LightDM with: `pkg install lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings`

LightDM and XFCE depends both on dbus so we enable it after boot and write it to /etc/rc.conf with: sysrc dbus_enable="YES"

To get LightDM running after boot we add it to /etc/rc.conf aswell with: `sysrc lightdm_enable="YES"`

To install XFCE you can use `pkg install xfce`.

To install XFCE with more 3 popular components you can use: `pkg install xfce xfce4-screensaver xfce4-screenshooter-plugin xfce4-taskmanager`

After installing you can reboot and login and use your desktop.

# Addtional Hints

To enabled FreeSync on FreeBSD you can read my post here: https://forums.freebsd.org/threads/howto-enable-variable-refresh-rate-freesync.79201/