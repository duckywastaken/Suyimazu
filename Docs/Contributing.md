Thank you for your interest in contributing to Suyimazu :)

To contribute to Suyimazu you can do the following:

- Submit new games or updates to the [library](https://codeberg.org/Alexander88207/Suyimazu/src/branch/main/Library)

- Submit new suggestions, your opinion, rating etc..

I am happy about every new idea or suggestion for improvement :cake: 