This handbook will help you to better understand Suyimazu.

# Functions

- Installation
- Launcher
- Uninstallation
- Winetricks
- Run a executable in prefix
- Update
- Manage Wine
- Applying a fix
- Open Suyimazu folder
- Enable logging
- Other
- About
- Exit


# Installation

With the function "Installation" the user can decide from a list of applications what he wants to install. Once an application has been selected, the installation of the selected application starts immediately. The application will be downloaded as good as possible from the original sources.

After downloading, changes to the prefix will be taken from the application, for instance installing DXVK or additional windows libraries. Then the installation of the application follows and done.

### Adding a custom installation

For people who want to add their own game there is the possibility to create a makefile and put that into the library folder.

# Launcher

The launcher allows you to launch the installed applications.

# Uninstallation

With the function "Uninstallation" the user can decide from a list of applications what he wants to uninstall. Once an application has been selected, the uninstallation of the selected application starts immediately. The prefix and the shortcuts of the selected application will be deleted.

# Winetricks

With this function the user can choose in which prefix he wants to open winetricks.

Little information about winetricks: 

Winetricks is a tool to fix or workaround problems in Wine.

# Run a executable in prefix

The function "Run a executable in prefix" allows to run a foreign Windows application in a selected prefix, this can be useful if you want to patch something or install an additional tool.

# Kill wine in a selected prefix

This function allows you to kill wine in a selected prefix.

If one programs blocks you hard by beeing freezed for example, then you can kill it easily without killing the other wine sessions.

# Update 

Allows you to update the installed application.

# Manage Wine

## Change Wine version for an application

Allows you to set a specific wine version for an application. This can be wine, wine-devel or wine-proton. But you can also set a custom one if you have one.

# Applying a fix

Allows you to apply a fix for a certain game.

# Open Suyimazu folder

Opens the path to Suyimazu, can be useful if you want to modify e.g. game files.

# Enable logging

If something doesn't work, you want to know why. The function "Enable logging" logs everything into a .log file which you can later retrieve under $HOME/.local/share/Suyimazu/Logs. This will also be necessary when creating a bug report.

# Other

## Use Software Rendering

This enables an temporary session where software rendering will be used. Can be useful to see if there is an specific graphic bug in an game which doesnt happen in software rendering for example.

## Use mesa_glthread

Mesa developers are looking for feedback about which games benefit from Mesa's glthread implementation (OpenGL multithreading), also known as threaded dispatch. The functionality is disabled by default, but games known to benefit from this feature are then whitelisted in global mesa configuration file.

Source: https://www.gamingonlinux.com/wiki/Performance_impact_of_Mesa_glthread

## Use Gallium HUD

This enables a temporary session with the Gallium HUD.

This can look like this: 

![GalliumHUD](https://codeberg.org/Alexander88207/Suyimazu/raw/branch/main/Media/GalliumHUD.png)

## Delete & reset everything

This deletes $ProjectPath and the shortcut.

## Codeberg page

Brings you to this repository.

## Update library

This delets the library folder and re-downloads it.

Please note that if you have custom stuff it to back it up.
# About

Gives a short description about the program and the license.

# Exit

Used to leave Suyimazu.